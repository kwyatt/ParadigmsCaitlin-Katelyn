console.log("Entered Script...");

document.getElementById("bsr-submit-button").onmouseup = callSubmitButton;

function callSubmitButton(){
	console.log("Inside beginning of callSubmitButton");
	
	// get all trivia details
	var level_val = document.getElementById("Level").value;
	var cat_val = document.getElementById("Category").value;
	var type_val = document.getElementById("Type").value;
	
	var question_box = document.getElementById("exampleInputQuestion").value;
	var rightAns_box = document.getElementById("exampleInputCAns").value;
	var wrongAns_box1 = document.getElementById("exampleInputWAns1").value;
	var wrongAns_box2 = document.getElementById("exampleInputWAns2").value;
	
	// create trivia info dictionary
	var trivia_dict = {};
	trivia_dict['level'] = level_val;
	trivia_dict['cat'] = cat_val;
	trivia_dict['type'] = type_val;
	trivia_dict['question'] = question_box;
	trivia_dict['correct'] = rightAns_box;
	trivia_dict['wrong1'] = wrongAns_box1;
	trivia_dict['wrong2'] = wrongAns_box2;
	
	// call display info
	displayInfo(trivia_dict);
}


function displayInfo(trivia_dict){
	console.log("Inside beginning of displayInfo...");
	
	var trivia_top = document.getElementById("trivia-top-line");
	trivia_top.innerHTML = 'Question is: ' + trivia_dict['question'] + '<br />' + 'Its specs are:<br /> Category: ' + trivia_dict['cat'] + '<br /> Type: ' + trivia_dict['type'] + '<br /> Difficulty: ' + trivia_dict['level'] + '<br /><br /> Possible Answers: <br />' + trivia_dict['correct'] + '<br />' + trivia_dict['wrong1'] + '<br />' + trivia_dict['wrong2'] + '<br />';
}
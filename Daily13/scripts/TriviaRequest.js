console.log("Entered Script...");

document.getElementById("submit-button").onmouseup = callSubmitButton;

function callSubmitButton(){
	console.log("Inside beginning of callSubmitButton");
	
	// get all trivia details
	var level_val = document.getElementById("Level").value;
	var cat_val = document.getElementById("Category").value;
	var type_val = document.getElementById("Type").value;
	var number_val = document.getElementById("Number").value;
	console.log(level_val);
	console.log(cat_val);
	console.log(type_val);
	console.log(number_val);
	
	
	// create trivia info dictionary
	var trivia_dict = {};
	makeNetworkCalltoApi(level_val, cat_val, type_val, number_val);

	
	// call display info
	displayInfo(trivia_dict);
}


function displayInfo(trivia_dict){
	console.log("Inside beginning of displayInfo...");	
}


function makeNetworkCalltoApi(level, cat, type, number) {
	console.log("Entered makeNetworkCalltoApi...");
	// https://opentdb.com/api.php?amount=10&category=9&difficulty=easy&type=multiple
	// set up url
	var xhr = new XMLHttpRequest();
	var url = 'https://opentdb.com/api.php?amount=' + number;
	var cat_number = 0;
	switch (cat){
		case 'Art':
			cat_number = 25;
			break;
		case 'Anime':
			cat_number = 31;
			break;
		case 'Books':
			cat_number = 10;
			break;
		case 'Random':
			cat_number = 0;
			break;
	}
	if (cat_number != 0) {
		url = url + '&category=' + str(cat_number);
	}
	if (level != 'Random'){
		url = url + '&difficulty=' + level.toLowerCase();
	}
	if (type != 'Random'){
		url = url + '&type=' + type.toLowerCase();
	}
	xhr.open("GET", url, true);
	
	// set up onload
	xhr.onload = function(e) {
		console.log(xhr.responseText);
		
		updateSiteWithResponse(xhr.responseText);
	}
	
	// set up onerror
	xhr.onerror = function(e) {
		console.error(xhr.statusText);
	}
	
	// make network call
	xhr.send(null);
	
}

function updateSiteWithResponse(response){
	console.log("Entered updateSiteWithResponse...");
	var response_json = JSON.parse(response);
	console.log(response_json['results']);
	
	var information = response_json['results'];
	
	var trivia_box = document.getElementById("questions_box");
	var j = 0;
	
	for (var i = 0; i < information.length; i++){
		j = i + 1;
		trivia_box.innerHTML = trivia_box.innerHTML + 'Question ' + j + ' is: ' + information[i]['question'] + '<br/>';
	}
	
	makeNetworkCalltoYodaApi(information[0]['question']);
	
}

function makeNetworkCalltoYodaApi(question) {
	console.log("Entered makeNetworkCalltoJokesApi...");
	
	var response = question.replace(/ /g, "%20");
	console.log(response);
	// set up url
	var xhr = new XMLHttpRequest();
	var urlYoda = 'https://api.funtranslations.com/translate/yoda.json?text=' + response;
	console.log(urlYoda);
	xhr.open("GET", urlYoda, true);
	
	// set up onload
	xhr.onload = function(e) {
		console.log(xhr.responseText);
		
		updateSiteWithYodaResponse(xhr.responseText);
	}
	
	// set up onerror
	xhr.onerror = function(e) {
		console.error(xhr.statusText);
	}
	
	// make network call
	xhr.send(null);
}


function updateSiteWithYodaResponse(response){
	console.log("Entered updateSiteWithJokeResponse...");
	var response_json = JSON.parse(response);
	
	var yoda_box = document.getElementById("yoda_box");
	var j = 0;
	
	yoda_box.innerHTML = response_json['contents']['translated'];

	
}

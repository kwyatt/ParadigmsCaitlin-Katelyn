import json
import cherrypy

class DictionaryController(object):
    '''Controller Class, which will hold event handler function'''

    def __init__(self):     # constructor
        self.myd = dict()

    def get_value(self, key):
        return self.myd[key]

    def add_entry(self, key, value):
        self.myd[key] = value

    # Event Handlers
    def GET_KEY(self, key):
        '''Event Handler for GET requests to /dictionary/:key with no body'''
        output = {'result' : 'success'}
        key = str(key)

        try:
            house = self.get_value(key)
            if house is not None:
                output['key'] = key
                output['value'] = house
            else:
                output['result'] = 'error'
                output['message'] = 'None type value associated with requested key'
        except KeyError as ex:
            output['result'] = 'error'
            output['message'] = 'key not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)


    def PUT_KEY(self, key):
        '''Event Handler for PUT request to /dictionary/:key with body {"value" : some_value}'''
        # 1. Create Default Response
        output = {'result' : 'success'}

        # 2. Cast key into correct type
        key = str(key)

        # 3. (optional) Read request body
        data_json = json.loads(cherrypy.request.body.read())

        # 4. In try blocks, do main operation
        try:
            house = data_json['value']
            self.add_entry(key, house)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        # 5. Return Response
        return json.dumps(output)
        
    def DELETE_INDEX(self):
        '''Event Handler for DELETE request to /dictionary/ without body'''
        # 1. Create Default Response
        output = {'result' : 'success'}

        # 2. Do main operation
        try:
            self.clear()
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        # 3. Return Response
        return json.dumps(output)

    def DELETE_KEY(self, key):
        '''Event Handler for DELETE request to /dictionary/ without body'''
        # 1. Create Default Response
        output = {'result' : 'success'}

        # 2. Cast key into correct type
        key = str(key)

        # 3. Do main operation
        try:
            del self.myd[key]
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        # 4. Return Response
        return json.dumps(output)
        
    def GET_INDEX(self):
        '''Event Handler for GET requests to /dictionary/ with no body'''
        output = {'result' : 'success'}

        try:
            output['entries'] = []
            for key_v, value_v in self.myd.items():
                entry_dict = {}
                #key_v = entry
                #value_v = self.myd[entry]
                entry_dict['key'] = key_v
                entry_dict['value'] = value_v
                output['entries'].append(entry_dict)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def POST_INDEX(self):
        '''Event Handler for POST request to /dictionary/ with body {"key : some_key, "value" : some_value}'''
        # 1. Create Default Response
        output = {'result' : 'success'}

        # 3. Read request body
        data_json = json.loads(cherrypy.request.body.read())

        # 4. In try blocks, do main operation
        try:
            key = data_json['key']
            val = data_json['value']
            self.add_entry(key, val)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        # 5. Return Response
        return json.dumps(output)

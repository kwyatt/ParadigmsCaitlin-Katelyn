import unittest
import requests
import json

class TestReset(unittest.TestCase):

    SITE_URL = 'http://student04.cse.nd.edu:51059' # replace with your port id
    print("Testing for server: " + SITE_URL)
    RESET_URL = SITE_URL + '/reset/'
    MOVIES_URL = SITE_URL + '/movies/'

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_put_reset_index(self):
        m = {}
        r = requests.put(self.RESET_URL)
	    #TODO complete this test

        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        
        m['title'] = 'Grumpier Old Men (1995)'
        m['genres'] = 'Comedy|Romance'
        mid = 3
        
        r = requests.get(self.MOVIES_URL + str(mid))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['title'], m['title'])
        self.assertEqual(resp['genres'], m['genres'])

        m['title'] = 'Now and Then (1995)'
        m['genres'] = 'Drama'
        mid = 27
        
        r = requests.get(self.MOVIES_URL + str(mid))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['title'], m['title'])
        self.assertEqual(resp['genres'], m['genres'])

        m['title'] = 'Friday (1995)'
        m['genres'] = 'Comedy'
        mid = 69
        
        r = requests.get(self.MOVIES_URL + str(mid))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['title'], m['title'])
        self.assertEqual(resp['genres'], m['genres'])


    def test_put_reset_key(self):
	    #TODO write this entire test
        m = {}
        m['title'] = 'Grumpier Old Men (1995)'
        m['genres'] = 'Comedy|Romance'
        mid = 3
        r = requests.put(self.RESET_URL + str(mid))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.MOVIES_URL + str(mid))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['title'], m['title'])
        self.assertEqual(resp['genres'], m['genres'])


	
	

if __name__ == "__main__":
    unittest.main()


Paradigms Final Project - Trivia
Katelyn Wyatt (kwyatt)
Caitlin Benyah (cbenyah)

SUMMARY:
Our API is intended to be used in a trivia website. This website will have two modes: Trivia (where the user will request the type and amount of trivia questions they want) and Add Your Own (where the user can input their own trivia question into the database).
The first page of this website will also give the user the ability to leave feedback on a particular trivia question if they think it is wrong in some small way or should be deleted overall.

Thus, the API has six main handlers: getting the trivia information requested by the user (GET_INDEX), getting the information on one question based on its ID (GET_KEY), adding a new trivia question from a user (POST_INDEX), editing an existing trivia question (PUT_KEY) for admin use only, reseting the database (PUT_INDEX)for admin use only and deleting an existing trivia question (DELETE_KEY) for admin use only.


OUR FILES:
Since our database is created using an online API (https://opentdb.com), we do not have a .dat file with our database information. Instead our server consists of:

trivia_library.py - this is where we load the data from opentdb.com and have the functions that edit our database and return information from it

trivia_controller.py - this is where we have the handlers from our server (mentioned above)

server.py - this is where our server is run and it includes the dispatchers

test_api.py - this file has all of the tests for the different handlers in our server


RUNNING THE SERVER:
In order to run the server you must first ensure that python3 is pointing to the right location. On the student machines this can be accomplished with "alias python3='/escnfs/home/csesoft/2017-fall/anaconda3/bin/python3'

Currently in both our server.py and test_api.py files the server is located at student02.cse.nd.edu. This will need to be changed to whatever student machine the server is being run on. Both of the terminals below need to be on this student machine.

Then, on one terminal run "python3 server.py"

On another terminal run "python3 test_api.py"

Note: Our test_api.py reloads the database for each test so if they seem to take a while this is why. This can be changed by simply removing the call to self.reset_data() at the beginning of each test. However, if you then run the tests twice in a row they may fail as the previous running of test_api.py will have deleted/edited some entries.

SCALE AND COMPLEXITY:
Half of the API’s categories and all of its types and levels were used. Thus, there were 78 unique types of questions (#cats * #types * #levels). The number questions varies as the specificiation vary as such for example the number of questions present in animals-easy-TF are different from animals-easy-MC.

With regards to complexity, a good majority came about when dealing with the Random vs. non-Random selection of questions, since the user has the choice of making any and all of the specifications random.
Moreover, adding new trivia to the correct portion of the database, formatting new information  into the database and onto the UI properly
added to the complexity. Lastly we had to account for the Interactivity/grading and connection between the two pages



JSON SPECS:
https://docs.google.com/spreadsheets/d/1CGus1MDorbY6EuEEAnlqxKBIQS80vWp3mi-nifKneGU/edit?usp=sharing

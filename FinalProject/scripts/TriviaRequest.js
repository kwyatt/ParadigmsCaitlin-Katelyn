console.log("Entered Script...");

document.getElementById("submit-button").onmouseup = callSubmitButton;
var amount_found = 0;
var information;
var response_json;
var amount_requested;

function callSubmitButton(){
	console.log("Inside beginning of callSubmitButton");
	
	// get all trivia details
	var level_val = document.getElementById("Level").value;
	var cat_val = document.getElementById("Category").value;
	var type_val = document.getElementById("Type").value;
	var number_val = document.getElementById("Number").value;
	console.log(level_val);
	console.log(cat_val);
	console.log(type_val);
	console.log(number_val);
	
	
	// request trivia from server
	makeNetworkCalltoApi(level_val, cat_val, type_val, number_val);

}

function makeNetworkCalltoApi(level, cat, type, number) {
	console.log("Entered makeNetworkCalltoApi...");
	// http://student04.cse.nd.edu/trivia/number=4&category=Animals&level=easy&type=boolean
	// set up url
	var xhr = new XMLHttpRequest();
	var url = 'http://student04.cse.nd.edu:51059/trivia/test/number=' + number;
	
	if (cat != 'Random') {
		url = url + '&category=' + cat;
	}
	if (level != 'Random'){
		url = url + '&level=' + level.toLowerCase();
	}
	if (type != 'Random'){
		url = url + '&type=' + type.toLowerCase();
	}
	console.log(url);
	xhr.open("GET", url, true);
	
	// set up onload
	xhr.onload = function(e) {
		console.log(xhr.responseText);
		
		updateSiteWithResponse(xhr.responseText, number);
	}
	
	// set up onerror
	xhr.onerror = function(e) {
		console.error(xhr.statusText);
	}
	
	// make network call
	xhr.send(null);
	
}

function updateSiteWithResponse(response, amount){
	console.log("Entered updateSiteWithResponse...");
	response_json = JSON.parse(response);
	amount_requested = amount;
	
	// Get Div tag for form
	var trivia_box = document.getElementById("questions_box");
	trivia_box.innerHTML = "";
	
	if ("questions" in response_json){
		console.log(response_json['questions']);
		information = response_json['questions'];

		var j = 0;
		amount_found = Object.keys(response_json['questions']).length;
		if (amount_found < amount){
			trivia_alert = document.createElement("p");
			trivia_alert.innerHTML = "We were only able to find " + amount_found.toString() + " questions for these specifications.";
			trivia_box.appendChild(trivia_alert);
		}
		
		// For each question
		for (var i = 0; i < information.length; i++){
			j = i + 1;
			
			// Make a label and append
			var question_label = document.createElement("label");
			question_label.innerHTML = j.toString() + ". " + information[i]['question'];
			var question_id_string = "question-" + i.toString();
			question_label.setAttribute("id", question_id_string);
			question_label.setAttribute("style", "font-size:20px");
			trivia_box.appendChild(question_label);		
			
			// For number of buttons
			if (information[i]['type'] == "boolean"){
				for (var k = 0; k < 2; k++){
					// Make a new div class and append main trivia_box
					var ans_div = document.createElement("div");
					ans_div.setAttribute("class", "radio");
					div_id_string = "radio-div-" + i.toString() + "-" + k.toString();	// div_id_string = radio-div-ques#-ans#
					ans_div.setAttribute("id", div_id_string);
					trivia_box.appendChild(ans_div);
					
					// make a label and append ^^
					var ans_label = document.createElement("label");
					label_id_string = "radio-label-" + i.toString() + "-" + k.toString();	// label_id_string = radio-label-ques#-ans#
					ans_label.setAttribute("id", label_id_string);
					ans_label.setAttribute("style", "font-size:20px");
					
					
					// make an input and append to ^^
					var ans_input = document.createElement("input");
					input_id_string = "radio-input-" + i.toString() + "-" + k.toString();	// input_id_string = radio-input-ques#-ans#
					input_name_string = "radio-input-" + i.toString();	// input_name_string = radio-input-ques#
					ans_input.setAttribute("id", input_id_string);
					ans_input.setAttribute("type", "radio");
					ans_input.setAttribute("value", input_id_string);
					ans_input.setAttribute("name", input_name_string);
					ans_input.setAttribute("style", "width:15px; height:15px");
					
					ans_label.appendChild(ans_input);
					if (k == 0) {
						ans_label.innerHTML = ans_label.innerHTML + "True";
					}
					else {
						ans_label.innerHTML = ans_label.innerHTML + "False";
					}
					ans_div.appendChild(ans_label);
				}
			}
			else {
				for (var k = 0; k < 4 ; k++){
// Make a new div class and append main trivia_box
					var ans_div = document.createElement("div");
					ans_div.setAttribute("class", "radio");
					div_id_string = "radio-div-" + i.toString() + "-" + k.toString();	// div_id_string = radio-div-ques#-ans#
					ans_div.setAttribute("id", div_id_string);
					trivia_box.appendChild(ans_div);
					
					// make a label and append ^^
					var ans_label = document.createElement("label");
					label_id_string = "radio-label-" + i.toString() + "-" + k.toString();	// label_id_string = radio-label-ques#-ans#
					ans_label.setAttribute("id", label_id_string);
					ans_label.setAttribute("style", "font-size:20px");
					
					
					// make an input and append to ^^
					var ans_input = document.createElement("input");
					input_id_string = "radio-input-" + i.toString() + "-" + k.toString();	// input_id_string = radio-input-ques#-ans#
					input_name_string = "radio-input-" + i.toString();	// input_name_string = radio-input-ques#
					ans_input.setAttribute("id", input_id_string);
					ans_input.setAttribute("type", "radio");
					ans_input.setAttribute("value", input_id_string);
					ans_input.setAttribute("name", input_name_string);
					ans_input.setAttribute("style", "width:15px; height:15px");
					
					ans_label.appendChild(ans_input);
					ans_label.innerHTML = ans_label.innerHTML + information[i]['answers'][k];
					ans_div.appendChild(ans_label);
					
					
				}
			}

		}
		
		// Submit Button
		var trivia_submit = document.createElement("button");
		trivia_submit.setAttribute("type", "button");
		trivia_submit.setAttribute("id", "trivia-submit-button");
		trivia_submit.setAttribute("class", "btn btn-primary");
		trivia_submit.setAttribute("style", "margin:5px;");
		trivia_submit.innerHTML = "Check Answers!";
		trivia_box.appendChild(trivia_submit);
		
		document.getElementById("trivia-submit-button").onmouseup = callTriviaSubmitButton;
		
		
		// Reset Button
		var trivia_reset = document.createElement("button");
		trivia_reset.setAttribute("type", "button");
		trivia_reset.setAttribute("id", "trivia-reset-button");
		trivia_reset.setAttribute("class", "btn btn-primary");
		trivia_reset.setAttribute("style", "margin:5px;");
		trivia_reset.innerHTML = "Reset Questions";
		trivia_box.appendChild(trivia_reset);
		
		document.getElementById("trivia-reset-button").onmouseup = callTriviaResetButton;
		
		
	}
	else {
		trivia_alert = document.createElement("p");
		trivia_alert.innerHTML = "No trivia questions found.";
		trivia_box.appendChild(trivia_alert);
	}

	
}

function callTriviaSubmitButton(){
	console.log("Entered callTriviaSubmitButton...");
	console.log("Amount of questions found: " + amount_found.toString());
	var count_correct = 0;
	var count_wrong = 0;
	
	// for each question
	for (var i = 0; i < amount_found; i++) {
		// check which answer is checked
		querySelectorString = 'input[name="radio-input-' + i.toString() + '"]:checked';
		console.log("Query Selector String: " + querySelectorString);
		var choice_object = document.querySelector(querySelectorString);
		if (choice_object != null) {
			var choice_id = choice_object.value;
			console.log("Choice: " + choice_id);
			// choice_id_list = ["radio", "input", "question#", "answer#"]
			var choice_id_list = choice_id.split("-");
			var choice_number = parseInt(choice_id_list[3]);
			var answer_choice = information[i]['answers'][choice_number];
		
			var question_label = document.getElementById("question-" + i.toString());
		
			var question_image = document.createElement("IMG");
			question_image.setAttribute("height", "50px");
			question_image.setAttribute("width", "50px");

			if (answer_choice == response_json['answers'][i]) {
				console.log("Question " + i.toString() + " Yay!");
				question_image.setAttribute("src", "images/correct.gif");
				count_correct += 1;
			}
			else{
				console.log("Question " + i.toString() + " :(");
				question_image.setAttribute("src", "images/wrong.gif");
				count_wrong += 1;
			}
			question_label.appendChild(question_image);
		}
		else {
			var question_label = document.getElementById("question-" + i.toString());
			var question_image = document.createElement("IMG");
			question_image.setAttribute("height", "50px");
			question_image.setAttribute("width", "50px");
			console.log("Question " + i.toString() + " :(");
			question_image.setAttribute("src", "images/wrong.gif");
			question_label.appendChild(question_image);
			count_wrong += 1;
		}
		
		
	}
	
	// Add score paragraph to top of div
	var score_paragraph = document.getElementById("score-report");
	score_paragraph.setAttribute("style", "font-size:20px");
	var score_string = count_correct.toString() + " Correct Out Of " + amount_found.toString();
	score_paragraph.innerHTML = score_string.bold();
	
}

function callTriviaResetButton() {
	console.log("Entered callTriviaResetButton...");
	
	// Get Div tag for form
	var trivia_box = document.getElementById("questions_box");
	trivia_box.innerHTML = "";
	var score_paragraph = document.getElementById("score-report");
	score_paragraph.innerHTML = "";
	
	if ("questions" in response_json){
		console.log(response_json['questions']);
		information = response_json['questions'];

		var j = 0;
		amount_found = Object.keys(response_json['questions']).length;
		if (amount_found < amount_requested){
			trivia_alert = document.createElement("p");
			trivia_alert.innerHTML = "We were only able to find " + amount_found.toString() + " questions for these specifications.";
			trivia_box.appendChild(trivia_alert);
		}
		
		// For each question
		for (var i = 0; i < information.length; i++){
			j = i + 1;
			
			// Make a label and append
			var question_label = document.createElement("label");
			question_label.innerHTML = j.toString() + ". " + information[i]['question'];
			var question_id_string = "question-" + i.toString();
			question_label.setAttribute("id", question_id_string);
			question_label.setAttribute("style", "font-size:20px");
			trivia_box.appendChild(question_label);		
			
			// For number of buttons
			if (information[i]['type'] == "boolean"){
				for (var k = 0; k < 2; k++){
					// Make a new div class and append main trivia_box
					var ans_div = document.createElement("div");
					ans_div.setAttribute("class", "radio");
					div_id_string = "radio-div-" + i.toString() + "-" + k.toString();	// div_id_string = radio-div-ques#-ans#
					ans_div.setAttribute("id", div_id_string);
					trivia_box.appendChild(ans_div);
					
					// make a label and append ^^
					var ans_label = document.createElement("label");
					label_id_string = "radio-label-" + i.toString() + "-" + k.toString();	// label_id_string = radio-label-ques#-ans#
					ans_label.setAttribute("id", label_id_string);
					ans_label.setAttribute("style", "font-size:20px");
					
					
					// make an input and append to ^^
					var ans_input = document.createElement("input");
					input_id_string = "radio-input-" + i.toString() + "-" + k.toString();	// input_id_string = radio-input-ques#-ans#
					input_name_string = "radio-input-" + i.toString();	// input_name_string = radio-input-ques#
					ans_input.setAttribute("id", input_id_string);
					ans_input.setAttribute("type", "radio");
					ans_input.setAttribute("value", input_id_string);
					ans_input.setAttribute("name", input_name_string);
					ans_input.setAttribute("style", "width:15px; height:15px");
					
					ans_label.appendChild(ans_input);
					if (k == 0) {
						ans_label.innerHTML = ans_label.innerHTML + "True";
					}
					else {
						ans_label.innerHTML = ans_label.innerHTML + "False";
					}
					ans_div.appendChild(ans_label);
				}
			}
			else {
				for (var k = 0; k < 4 ; k++){
// Make a new div class and append main trivia_box
					var ans_div = document.createElement("div");
					ans_div.setAttribute("class", "radio");
					div_id_string = "radio-div-" + i.toString() + "-" + k.toString();	// div_id_string = radio-div-ques#-ans#
					ans_div.setAttribute("id", div_id_string);
					trivia_box.appendChild(ans_div);
					
					// make a label and append ^^
					var ans_label = document.createElement("label");
					label_id_string = "radio-label-" + i.toString() + "-" + k.toString();	// label_id_string = radio-label-ques#-ans#
					ans_label.setAttribute("id", label_id_string);
					ans_label.setAttribute("style", "font-size:20px");
					
					
					// make an input and append to ^^
					var ans_input = document.createElement("input");
					input_id_string = "radio-input-" + i.toString() + "-" + k.toString();	// input_id_string = radio-input-ques#-ans#
					input_name_string = "radio-input-" + i.toString();	// input_name_string = radio-input-ques#
					ans_input.setAttribute("id", input_id_string);
					ans_input.setAttribute("type", "radio");
					ans_input.setAttribute("value", input_id_string);
					ans_input.setAttribute("name", input_name_string);
					ans_input.setAttribute("style", "width:15px; height:15px");
					
					ans_label.appendChild(ans_input);
					ans_label.innerHTML = ans_label.innerHTML + information[i]['answers'][k];
					ans_div.appendChild(ans_label);
					
					
				}
			}

		}
		
		// Submit Button
		var trivia_submit = document.createElement("button");
		trivia_submit.setAttribute("type", "button");
		trivia_submit.setAttribute("id", "trivia-submit-button");
		trivia_submit.setAttribute("class", "btn btn-primary");
		trivia_submit.setAttribute("style", "margin:5px;");
		trivia_submit.innerHTML = "Check Answers!";
		trivia_box.appendChild(trivia_submit);
		
		document.getElementById("trivia-submit-button").onmouseup = callTriviaSubmitButton;
		
		
		// Reset Button
		var trivia_reset = document.createElement("button");
		trivia_reset.setAttribute("type", "button");
		trivia_reset.setAttribute("id", "trivia-reset-button");
		trivia_reset.setAttribute("class", "btn btn-primary");
		trivia_reset.setAttribute("style", "margin:5px;");
		trivia_reset.innerHTML = "Reset Questions";
		trivia_box.appendChild(trivia_reset);
		
		document.getElementById("trivia-reset-button").onmouseup = callTriviaResetButton;
		
		
	}
	else {
		trivia_alert = document.createElement("p");
		trivia_alert.innerHTML = "No trivia questions found.";
		trivia_box.appendChild(trivia_alert);
	}
}






console.log("Entered Script...");

document.getElementById("bsr-submit-button").onmouseup = callSubmitButton;

function callSubmitButton(){
	console.log("Inside beginning of callSubmitButton");
	
	// get all trivia details
	var level_val = document.getElementById("Level").value;
	var cat_val = document.getElementById("Category").value;
	var type_val = document.getElementById("Type").value;
	
	var question_box = document.getElementById("exampleInputQuestion").value;
	var rightAns_box = document.getElementById("exampleInputCAns").value;
	var wrongAns_box1 = document.getElementById("exampleInputWAns1").value;
	var wrongAns_box2 = document.getElementById("exampleInputWAns2").value;
	var wrongAns_box3 = document.getElementById("exampleInputWAns3").value;
	
	// create trivia info dictionary
	var trivia_dict = {};
	trivia_dict['level'] = level_val;
	trivia_dict['category'] = cat_val;
	trivia_dict['type'] = type_val;
	trivia_dict['question'] = question_box;
	trivia_dict['incorrect_answers'] = [];
	if (type_val == "boolean"){
		trivia_dict['correct_answer'] = rightAns_box;
		trivia_dict['incorrect_answers'].push(wrongAns_box1);
	}
	else {
		trivia_dict['correct_answer'] = rightAns_box;
		trivia_dict['incorrect_answers'].push(wrongAns_box1);
		trivia_dict['incorrect_answers'].push(wrongAns_box2);
		trivia_dict['incorrect_answers'].push(wrongAns_box3);
	}
	
	// request trivia from server
	makeNetworkCalltoApi(trivia_dict);
	
	// call display info
	displayInfo(trivia_dict);
}

function makeNetworkCalltoApi(trivia_dict) {
	console.log("Entered makeNetworkCalltoApi...");
	// http://student04.cse.nd.edu/trivia/number=4&category=Animals&level=easy&type=boolean
	// set up url
	var xhr = new XMLHttpRequest();
	var url = 'http://student04.cse.nd.edu:51059/trivia/';
	console.log(url);
	xhr.open("POST", url, true);
	
	// set up onload
	xhr.onload = function(e) {
		console.log(xhr.responseText);
		
		// TODO: Alert the user it was added. Display question?
	}
	
	// set up onerror
	xhr.onerror = function(e) {
		console.error(xhr.statusText);
	}
	
	// make network call
	xhr.send(JSON.stringify(trivia_dict));
	
}



function displayInfo(trivia_dict){
	console.log("Inside beginning of displayInfo...");
	var trivia_top = document.getElementById("trivia-top-line");
	
	if (trivia_dict['type'] == "multiple") {
		trivia_top.innerHTML = 'Question is: ' + trivia_dict['question'] + '<br />' + 'Its specs are:<br /> Category: ' + trivia_dict['category'] + '<br /> Type: ' + trivia_dict['type'] + '<br /> Difficulty: ' + trivia_dict['level'] + '<br /><br /> Possible Answers: <br />' + trivia_dict['correct_answer'] + '<br />' + trivia_dict['incorrect_answers'][0] + '<br />' + trivia_dict['incorrect_answers'][1] + '<br />' + trivia_dict['incorrect_answers'][2] + '<br />';
	}
	else {
		trivia_top.innerHTML = 'Question is: ' + trivia_dict['question'] + '<br />' + 'Its specs are:<br /> Category: ' + trivia_dict['category'] + '<br /> Type: ' + trivia_dict['type'] + '<br /> Difficulty: ' + trivia_dict['level'] + '<br /><br /> Possible Answers: <br />' + trivia_dict['correct_answer'] + '<br />' + trivia_dict['incorrect_answers'][0] + '<br />';
	}
}






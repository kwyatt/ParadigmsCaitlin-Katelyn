import requests
import json
import math
import random

class _trivia_database:
    
    def __init__(self):
        self.next_t_id = 1
        self.tdb = dict()

        self.categories = {
                    "Animals":27,
                    "AnimeandManga":31,
                    "Books":10,
                    "Film":11,
                    "GeneralKnowledge":9,
                    "History":23,
                    "Mathematics":10,
                    "Music":12,
                    "Mythology":20,
                    "Politics":24,
                    "ScienceandNature":17,
                    "Sports":10,
                    "Video Games":15}
        self.levels = ["easy", "medium", "hard"]
        self.t_types = ["multiple", "boolean"]


    def get_key(self, cat, t_type, level):
        '''switch statement for database'''
        return cat + "-" + t_type + "-" + level

    def load_trivia(self):
        '''Load trivia questions into personal database'''
        '''database is a dictionary of 'id':'question_info' '''
        '''the id of the trivia question will have it's most significant digit be different based on category'''
           
        # Query each category for 50 questions and save in database
        for cat, num in self.categories.items():
            for lev in self.levels:
                for ty in self.t_types:
                    # Query API for questions NOTE: Not every query is reading in values
                    url = 'https://opentdb.com/api.php?amount=10&category=' + str(num) + '&difficulty=' + lev + '&type=' + ty
                    resp = requests.get(url)
                    resp = json.loads(resp.content.decode())

                    # Parse and save using next_t_id
                    for result in resp['results']:
                        result['level'] = result['difficulty']
                        del result['difficulty']
                        #print(result)
                        key = self.get_key(cat, ty, lev)
                        if not key in self.tdb:
                            self.tdb[key] = dict()
                        self.tdb[key][str(self.next_t_id)] = result   # add question json to database
                    
                        # increment next_t_id
                        self.next_t_id += 1


    def get_questions(self, specs):
        '''choose the right function to handle the specs we have'''

        if len(specs) == 4:
            questions, answers = self.get_questions_allSpecs(specs)
        elif len(specs) == 3:
            questions, answers = self.get_questions_twoSpecs(specs)
        elif len(specs) == 2:
            questions, answers = self.get_questions_singleSpec(specs)
        else:       # if no specifications (all random)
            questions, answers = self.get_questions_noSpecs(specs)

        return questions, answers

    
    
    def get_question(self, t_id):
        '''return a singular question based on t_id'''
        for key, value in self.tdb.items():
            if t_id in value:
                return self.tdb[key][t_id]

        return {}




    ### GET_QUESTION FUNCTIONS ###
    def get_questions_allSpecs(self, specs):
        '''Return only the questions and answer choices'''
        '''specs is list of specifications = [category, type, amount, difficulty]'''
        cat = specs['category']
        t_type = specs['type']
        level = specs['level']

        key = self.get_key(cat, t_type, level)

        # Initialize Output
        all_questions = []
        all_correct_answers = []
        counter_goal = int(specs['number'])
        cur_counter = 1


        # Iterate Through Database
        for index, ques in self.tdb[key].items():   
            if cur_counter > counter_goal:
                break
            
            question = ques['question']          # question is a string
            answers = ques['incorrect_answers']  # answers is a list of strings
            answers.append(ques['correct_answer'])
            print(question)
            print(answers)
            random.shuffle(answers)

            # Make Entry
            entry = {'t_id':index,
                     'category':cat,
                     'level':level,
                     'type':t_type,
                     'question':question,
                     'answers':answers}
            
            all_questions.append(entry)
            all_correct_answers.append(ques['correct_answer'])

            cur_counter += 1

        return all_questions, all_correct_answers

    def get_questions_twoSpecs(self, specs):
        '''Return only the questions and answer choices'''
        '''specs is list of specifications = [category, type, amount, difficulty]'''
        if 'category' in specs.keys():
            cat = specs['category']
        else:
            cat, value = random.choice(list(self.categories.items()))

        if 'level' in specs.keys():
            level = specs['level']
        else:
            level = random.choice(self.levels)

        if 'type' in specs.keys():
            t_type = specs['type']
        else:
            t_type = random.choice(self.t_types)

        key = self.get_key(cat, t_type, level)

        # Initialize Output
        all_questions = []
        all_correct_answers = []
        counter_goal = int(specs['number'])
        cur_counter = 1


        # Iterate Through Database
        for index, ques in self.tdb[key].items():   
            if cur_counter > counter_goal:
                break
            
            question = ques['question']          # question is a string
            answers = ques['incorrect_answers']  # answers is a list of strings
            answers.append(ques['correct_answer'])
            print(question)
            print(answers)
            random.shuffle(answers)

            # Make Entry
            entry = {'t_id':index,
                     'category':cat,
                     'level':level,
                     'type':t_type,
                     'question':question,
                     'answers':answers}
            
            all_questions.append(entry)
            all_correct_answers.append(ques['correct_answer'])

            cur_counter += 1

        return all_questions, all_correct_answers


    def get_questions_singleSpec(self, specs):
        '''Return only the questions and answer choices'''
        '''specs is list of specifications = [category, type, amount, difficulty]'''
        # Determine which spec we are checking for
        if 'type' in specs.keys():
            check = 'type'
        elif 'level' in specs.keys():
            check = 'level'
        elif 'category' in specs.keys():
            check = 'category'

        # Initialize Output
        all_questions = []
        all_correct_answers = []
        counter_goal = int(specs['number'])
        cur_counter = 1

        # For random questions
        while cur_counter <= counter_goal:

            # Get random question
            options = random.choice(list(self.tdb.values()))
            key, choice = random.choice(list(options.items()))  # get random question info

            # Check if question has correct specification
            if choice[check] != specs[check]:
                continue

            # Check if question already used
            found = False
            for info in all_questions:
                if info['question'] == choice['question']:
                    found = True
                    break
            if found:
                continue

            # Make Entry
            answers = choice['incorrect_answers']
            answers.append(choice['correct_answer'])
            random.shuffle(answers)
            entry = {'t_id':key,
                     'category':choice['category'],
                     'level':choice['level'],
                     'type':choice['type'],
                     'question':choice['question'],
                     'answers':answers}

            all_questions.append(entry)
            all_correct_answers.append(choice['correct_answer'])

            cur_counter += 1

        return all_questions, all_correct_answers

        

    def get_questions_noSpecs(self, specs):
        '''Return only the questions and answer choices'''
        '''specs is list of specifications = [category, type, amount, difficulty]'''

        # Initialize Output
        all_questions = []
        all_correct_answers = []
        counter_goal = int(specs['number'])
        cur_counter = 1

        # For random questions
        while cur_counter <= counter_goal:
            print(cur_counter)

            # Get random question
            options = random.choice(list(self.tdb.values()))
            key, choice = random.choice(list(options.items()))  # get random question info

            # Check if question already used
            found = False
            for info in all_questions:
                if info['question'] == choice['question']:
                    found = True
                    break
            if found:
                continue

            # Make Entry
            answers = choice['incorrect_answers']
            answers.append(choice['correct_answer'])
            random.shuffle(answers)
            entry = {'t_id':key,
                     'category':choice['category'],
                     'level':choice['level'],
                     'type':choice['type'],
                     'question':choice['question'],
                     'answers':answers}

            all_questions.append(entry)
            all_correct_answers.append(choice['correct_answer'])
            
            cur_counter += 1

        return all_questions, all_correct_answers



    ### END OF GET_QUESTION FUNCTIONS ###


    def add_trivia(self, specs):
        '''Add trivia question to database'''
        '''specs is list of specifications = [question, category, type, correct answer, incorrect answer(s), difficulty]'''
        
        # Create a trivia object and key based on specs
        cat = specs['category']
        t_type = specs['type']
        level = specs['level']

        key = self.get_key(cat, t_type,level)
        self.tdb[key][self.next_t_id] = specs
        self.next_t_id += 1
          
        # TODO: output the correct thing (t_id)
        return self.next_t_id-1

    def edit_trivia(self, t_id, specs):
        '''Edit existing trivia question in database'''
        '''t_id is the id of the specific trivia question'''
        #so they pass in all the specs and not just the singluar spec that they wanna add
        
        for key, value in self.tdb.items():
            if t_id in value:
                for key_spec, spec in specs.items():
                    if spec:
                        self.tdb[key][t_id][key_spec] = spec


    def delete_trivia(self, t_id):
        '''Delete an existing trivia question based on t_id'''
        for key, value in self.tdb.items():
            if t_id in value: 
                del self.tdb[key][t_id]
                break
            





if __name__ == "__main__":
    db = _trivia_database()

    # Test get_key
    key = db.get_key("music", "mc", "easy")
    print(key)

    # Load Trivia
    db.load_trivia()
    '''
    for key, value in db.tdb.items():
        print(key)
    print()
    for key, value in db.tdb.items():
        print(key)
        print(value)
        print()
        print(value["4"])
        break

    # Test get_questions
    # Get question (singular)
    info = db.get_question("1")
    print(info)

    # All Specs
    specs = {'category': 'Animals',
             'type': 'boolean',
             'level': 'easy',
             'number':4}
    questions, answers = db.get_questions(specs)
    print(questions)
    print(answers)

    # No Level
    specs = {'category': 'Animals',
             'type': 'multiple',
             'number':4}
    questions, answers = db.get_questions(specs)
    print(questions)
    print(answers)

    # No Type
    specs = {'category': 'Animals',
             'level': 'easy',
             'number':4}
    questions, answers = db.get_questions(specs)
    print(questions)
    print(answers)
    
    # No Category
             'type': 'multiple',
             'level': 'easy',
             'number':4}
    questions, answers = db.get_questions(specs)
    print(questions)
    print(answers)

    # Single Spec
    specs = {'type': 'boolean',
             'number':4}
    questions, answers = db.get_questions(specs)
    for ques in questions:
        print(ques['answers'])
    #print(answers)

    # No Spec
    specs = {'number':4}
    questions, answers = db.get_questions(specs)
    print(questions)
    print(answers)

    # Test add_trivia
    specs = {'category': 'Animals',
             'type': 'multiple',
             'level': 'easy',
             'question':'testing',
             'correct_answer':'right',
             'incorrect_answers': ['wrong1','wrong2','wrong3']}
    t_id = db.add_trivia(specs)
    check = db.get_question(t_id)
    print(check['question'])    # should be "testing"

    # Test edit_trivia
    specs = {'question':'testing round 2',
             'correct_answer':'',
             'incorrect_answers': []}
    db.edit_trivia(t_id, specs)
    check = db.get_question(t_id)
    print(check['question'])    # should be "testing round 2"


    # Test delete_trivia
    db.delete_trivia(t_id)
    check = db.get_question(t_id)
    print(check)    # Should be "{}"
    '''

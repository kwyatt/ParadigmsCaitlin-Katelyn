import json
import cherrypy
from trivia_library import _trivia_database

class TriviaController(object):
    '''Controller Class, which will hold event handler function'''

    def __init__(self):     # constructor
        self.mydb = _trivia_database()
        self.mydb.load_trivia()


    def get_value(self, key):
        return self.mydb[key]


    def add_entry(self, key, value):
        self.mydb[key] = value



    # Event Handlers
    def GET_INDEX(self, url):
        '''when GET request for /trivia/ comes in, respond with trivia questions in json string'''
        output = {'result':'success'}
        specs = {}

        
        try:
            specsList = url.split('&')  # ['number=4','cat=Animals',...]
            for spec in specsList:
                key, value = spec.split('=')    # key = 'number', value = '4'
                specs[key] = value

            # Read in specs
            questions, answers = self.mydb.get_questions(specs)       # questions is a list of json, answers is list of strings

            output['questions'] = questions
            output['answers'] = answers

            if len(questions) < int(specs['number']):
                output['message'] = 'This is the maximum amount of data we have. Feel free to add more.'

        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        
        return json.dumps(output)

    
    def GET_KEY(self, t_id):
        '''when GET request for /trivia/t_id comes in, return info on that question'''
        output = {'result':'success'}
        t_id = str(t_id)

        try:
            information = self.mydb.get_question(t_id)
            if not information:
                output['message'] = 'This question does not exist.'
                output['result'] = 'failure'
            else:
                output['information'] = information
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        

        return json.dumps(output)

    
    def PUT_KEY(self, t_id):
        '''when PUT request for /trivia/t_id comes in, change that question in db'''
        output = {'result':'success'}
        t_id = str(t_id)

        try:
            specs = json.loads(cherrypy.request.body.read().decode('utf-8'))

            self.mydb.edit_trivia(t_id, specs)
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        

        return json.dumps(output)
      

    def PUT_INDEX(self):
        '''when PUT request for /trivia/reset/ comes in, reset the database'''
        output = {'result':'success'}

        try:

            self.mydb = _trivia_database()
            self.mydb.load_trivia()
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        

        return json.dumps(output)
        

    def POST_INDEX(self):
        '''when POST request for /trivia/ comes in, add that question to db'''
        output = {'result':'success'}

        try:
            specs = json.loads(cherrypy.request.body.read().decode('utf-8'))

            t_id = self.mydb.add_trivia(specs)
            output['t_id'] = t_id
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
       

    def DELETE_KEY(self, t_id):
        '''when DELETE request for /trivia/t_id comes in, delete that question in db'''
        output = {'result':'success'}
        t_id = str(t_id)

        try:

            self.mydb.delete_trivia(t_id)
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        

        return json.dumps(output)

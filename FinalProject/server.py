import cherrypy
import routes
from trivia_controller import TriviaController

class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""


def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"


def start_service():
    '''Configures and Runs Server'''
    tCon = TriviaController()
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    # Dispatchers
    dispatcher.connect('dict_get_index', '/trivia/test/:url', controller=tCon, action='GET_INDEX', conditions=dict(method=['GET']))
    dispatcher.connect('dict_get_key', '/trivia/:t_id', controller=tCon, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('dict_put_key', '/trivia/:t_id', controller=tCon, action='PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('dict_put_index', '/trivia/reset/', controller=tCon, action='PUT_INDEX', conditions=dict(method=['PUT']))
    dispatcher.connect('dict_post_index', '/trivia/', controller=tCon, action='POST_INDEX', conditions=dict(method=['POST']))
    dispatcher.connect('dict_delete_key', '/trivia/:t_id', controller=tCon, action='DELETE_KEY', conditions=dict(method=['DELETE']))

    # Dispatchers for CORS
    dispatcher.connect('trivia_key_options', '/trivia/:t_id', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('trivia_options', '/trivia/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('trivia_test_options', '/trivia/test/:url', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_options', '/trivia/reset/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS'])) 

    conf = {
            'global':{
                    'server.thread_pool' : 5,
                    'server.socket_host' : 'student04.cse.nd.edu',   # 'localhost'
                    'server.socket_port' : 51059,
                },
            '/' : {
                'request.dispatch' : dispatcher,    # object
                'tools.CORS.on' : True,
                }
            }
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)





if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize',CORS)
    start_service()

import unittest 
import requests 
import json 

class TestApi(unittest.TestCase):
    
    SITE_URL = 'http://student02.cse.nd.edu:51059'

    print("Testing for server: " + SITE_URL)

    TRIVIA_URL = SITE_URL + '/trivia/'
    RESET_URL = TRIVIA_URL + 'reset/'

    def reset_data(self):
        body = {}
        r = requests.put(self.RESET_URL, data = json.dumps(body))  
        resp = json.loads(r.content.decode())
      
    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False
    
    
    def test_trivia_index_get(self):
        self.reset_data()

        urlSpecs = 'number=4&category=Animals&level=easy&type=boolean'
        r = requests.get(self.TRIVIA_URL + '/test/' + urlSpecs)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        check = True
        for index, trivia in enumerate(resp['questions']):
            self.assertEqual(trivia['category'], 'Animals')
            self.assertEqual(trivia['level'], 'easy')
            self.assertEqual(trivia['type'], 'boolean')
            answers = trivia['answers']
            if not resp['answers'][index] in answers:
                check = False
        self.assertEqual(check, True)
    

    def test_trivia_key_get(self):
        self.reset_data()

        r = requests.get(self.TRIVIA_URL + str(1))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')


    def test_trivia_index_post(self):
        self.reset_data()

        body = {}
        body['category'] = 'History'
        body['level'] = 'easy'
        body['type'] = 'boolean'
        body['question'] = 'The capital of the US is Washington D.C.'
        body['correct_answer'] = 'True'
        body['incorrect_answers'] = ['False']

        r = requests.post(self.TRIVIA_URL, data = json.dumps(body))
        self.assertTrue(self.is_json(r.content.decode()))
        
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        # Check that t_id is added
        r = requests.get(self.TRIVIA_URL + str(resp['t_id']))
        self.assertTrue(self.is_json(r.content.decode()))
        resp_during = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
      
    
    def test_trivia_key_put(self):
        self.reset_data()

        body = {}
        body['question'] = 'testing'
        body['correct_answer'] = ''
        body['incorrect_answers'] = ['']
        test_id = 12
       
        # Make the edit request
        r = requests.put(self.TRIVIA_URL + str(test_id), data = json.dumps(body))
        self.assertTrue(self.is_json(r.content.decode()))
        resp_during = json.loads(r.content.decode())
        self.assertEqual(resp_during['result'], 'success')

        # Check that it changed
        r = requests.get(self.TRIVIA_URL + str(test_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp_after = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp_after['information']['question'], 'testing')

    
    def test_trivia_key_delete(self):
        self.reset_data()

        t_id = 10
        r = requests.get(self.TRIVIA_URL + str(t_id))
        self.assertTrue(self.is_json(r.content.decode()))
        resp_before = json.loads(r.content.decode())
        self.assertEqual(resp_before['result'], 'success')

        # delete it
        body = {}
        r = requests.delete(self.TRIVIA_URL + str(t_id), data = json.dumps(body))
        self.assertTrue(self.is_json(r.content.decode()))
        resp_during = json.loads(r.content.decode())
        self.assertEqual(resp_during['result'], 'success')

        # Check it's deleted
        r = requests.get(self.TRIVIA_URL + str(t_id))
        self.assertTrue(self.is_json(r.content.decode()))
        resp_after = json.loads(r.content.decode())
        self.assertEqual(resp_after['result'], 'failure')
        self.assertEqual(resp_after['message'], 'This question does not exist.')



if __name__ == "__main__":
    unittest.main()

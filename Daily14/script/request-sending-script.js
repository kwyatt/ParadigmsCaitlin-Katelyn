console.log("Entered script...");

document.getElementById("send-button").onmouseup = callSubmitButton;

function callSubmitButton(){
	console.log("Inside beginning of callSubmitButton");
	
	// get all details
	// address
	var address_num = document.getElementById("select-server-address").selectedIndex;
	var address = document.getElementById('select-server-address').options[address_num].value;

	// port
	var port = document.getElementById("input-port-number").value;
	
	// Request type
	var type = "GET"
	if (document.getElementById('radio-get').checked){
        type = "GET";
	} else if (document.getElementById('radio-put').checked) {
        type = "PUT";
	} else if (document.getElementById('radio-post').checked) {
        type = "POST";
	} else if (document.getElementById('radio-delete').checked) {
        type = "DELETE";
	}
	// if key clicked, get key
	var key = null;
	if (document.getElementById('checkbox-use-key').checked){
		key = document.getElementById('input-key').value;
	}
	// if message clicked, get message
	var message = null;
	if (document.getElementById('checkbox-use-message').checked){
		message = document.getElementById('text-message-body').value;
	}
	
	console.log(address);
	console.log(key);
	console.log(message);
	console.log(type);
	
	
	// create movie info dictionary
	var movie_dict = {};
	movie_dict["address"] = address;
	movie_dict["port"] = port;
	movie_dict["type"] = type;
	movie_dict["key"] = key;
	movie_dict["body"] = message;
	
	
	makeNetworkCalltoApi(movie_dict);
}


function makeNetworkCalltoApi(movie_dict) {
	console.log("Entered makeNetworkCalltoApi...");
	
	// Make url
	var xhr = new XMLHttpRequest();
	if (movie_dict["key"] != null) {
		var url = movie_dict["address"] + ":" + movie_dict["port"] + "/movies/" + movie_dict["key"];
	}
	else {
		var url = movie_dict["address"] + ":" + movie_dict["port"] + "/movies/";
	}
	xhr.open(movie_dict["type"], url, true);
	
	// set up onload
	xhr.onload = function(e) {
		console.log(xhr.responseText);
		
		updateSiteWithResponse(xhr.responseText, movie_dict);
	}
	
	// set up onerror
	xhr.onerror = function(e) {
		console.error(xhr.statusText);
	}
	
	// make network call
	if (movie_dict["body"] != null && movie_dict["type"] != "GET") {
		xhr.send(movie_dict["body"]);
	}
	else{
		xhr.send(null);
	}
}

function updateSiteWithResponse(response, movie_dict) {
	console.log("Inside updateSiteWithResponse...");
	
	var response_json = JSON.parse(response);
	var answer_box = document.getElementById("answer-label");
	
	if (movie_dict["type"] == "GET" && movie_dict["key"] != null){
		answer_box.innerHTML = response + "<br />" + response_json["title"] + " belongs to the genres: " + response_json["genres"];
	}
	else if (movie_dict["type"] == "GET" && movie_dict["key"] == null){
		console.log("inside");
		answer_box.innerHTML = response + "<br />";
		// for loop
		for (var i = 0; i < 10; i++){
			answer_box.innerHTML = answer_box.innerHTML + response_json["movies"][i]["title"] + " belongs to the genres: " + response_json["movies"][i]["genres"] + "<br />";
		}
	}
	else {
		answer_box.innerHTML = response;
	}
}














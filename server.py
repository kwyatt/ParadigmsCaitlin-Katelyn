import cherrypy
import routes
from dictionary_controller import DictionaryController

def start_service():
    '''Configures and Runs Server'''
    dCon = DictionaryController()
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    dispatcher.connect('dict_get_key', '/dictionary/:key', controller=dCon, action='GET_KEY', conditions=dict(method=[$
    dispatcher.connect('dict_put_key', '/dictionary/:key', controller=dCon, action='PUT_KEY', conditions=dict(method=[$
    dispatcher.connect('dict_delete_index', '/dictionary/', controller=dCon, action='DELETE_INDEX', conditions=dict(me$
    dispatcher.connect('dict_delete_key', '/dictionary/:key', controller=dCon, action='DELETE_KEY', conditions=dict(me$
    dispatcher.connect('dict_get_index', '/dictionary/', controller=dCon, action='GET_INDEX', conditions=dict(method=[$
    dispatcher.connect('dict_post_index', '/dictionary/', controller=dCon, action='POST_INDEX', conditions=dict(method$

    # Configuration for the server
    conf = {
            'global':{
                    'server.socket_host' : 'student04.cse.nd.edu',   # 'localhost'
                    'server.socket_port' : 51059,
                },
            '/' : {
                'request.dispatch' : dispatcher,    # object
                }
            }
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)





if __name__ == '__main__':
    start_service()
